let player ={
    name : "Prajwal",
    chips : 145,
    sayHello: function(){
        console.log("hellooo");
    }
}

player.sayHello();

let cards = [];
let sum = 0;
let hasBlackJack = false
let isAlive = false;
let message = ""

let messageEl= document.getElementById("message-el");
//let sumEl = document.getElementById("sum-el");
let sumEl = document.querySelector("#sum-el");
let cardsEl = document.getElementById("cards-el");
let playerEl = document.getElementById("player-el");


playerEl.textContent =player.name + ": $" + player.chips;

function getRandomCard(){
    //if 1(ace) ->return 11
    //if 11(jack) or 12(queen) or 13(king) ->return 10
    let randomNumber= Math.floor(Math.random()*13) +1;

    if (randomNumber === 1) {
        return 11;
    }
    else if(randomNumber > 10){
        return 10;
    }else{
        return randomNumber;
    }
}

function startGame(){
    let firstCard = getRandomCard();
    let secondCard = getRandomCard();
    isAlive = true;
    sum = firstCard + secondCard;
    cards.push(firstCard);
    cards.push(secondCard);
    renderGame();
}

function renderGame() {
   // cardsEl.textContent = "Cards: "+ firstCard + " "+ secondCard;
    cardsEl.textContent = "Cards: ";
    for(let i=0; i< cards.length; i++){
        cardsEl.textContent += cards[i] + " ";

    }

    sumEl.textContent = "Sum: "+sum;
    if (sum <= 20) {
    message = "Do you want to draw a new card? 🙂"
} else if (sum === 21) {
    message = "Wohoo! You've got Blackjack! 🥳"
    hasBlackJack = true
} else {
    message = "You're out of the game! 😭"
    isAlive = false
}

messageEl.textContent = message;
console.log(message)
}

function newCard(){
    if (isAlive === true && hasBlackJack === false) {
       let newCard = getRandomCard();
    cards.push(newCard);
    sum += newCard;
    renderGame(); 
    }
    
}